#!/usr/bin/env python3
"""
Stubs for mpl_toolskits, a library for additional function to be included with
matplotlib.
"""

from .mplot import Mplot3D

mplot3d: Mplot3D
