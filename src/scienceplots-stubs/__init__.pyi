#!/usr/bin/env python3
"""
Init-file for the stub-file for the library SciencePlots, which is a dependency
of scientific_plots. This library must only be imported, but remains unused in
the code, except for the added "science" style. Thus, this module does not
actually contain any functions or classed.
"""
