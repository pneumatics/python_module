#!/usr/bin/env python3
"""
Stubs for mpl_toolskits' submodule mplot,
a library for additional function to be included with
matplotlib.
"""


class Axes:
    """Container for different axes."""

    class Axes3D:
        """
        3D-axes base class.
        """


class Mplot3D:
    """
    Class for the design of 3d surface plots.
    """

    axes3d: Axes
