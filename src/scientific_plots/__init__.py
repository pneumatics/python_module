#!/usr/bin/env python3
"""This is a python-module for scientific plotting.
It contains functions and methods, which are
useful across several different projects and repositories. It also contains
stub-files with several data-type annotations for scipy and matplot-lib
functions."""

__version__ = "1.8.7"
