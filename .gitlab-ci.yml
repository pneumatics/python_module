# this is a gitlab-pipeline for this project
# it analysis the shell scripts as well as the python-code
---
image: python:3.10

variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
    paths:
        - .cache/pip

stages:
    - test
    - static analysis
    - testing
    - build
    - publish

include:
    - template: Security/Secret-Detection.gitlab-ci.yml

secret_detection:
    variables:
        SECRET_DETECTION_HISTORIC_SCAN: "true"

.python: &python
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install "."

pyright:
    stage: static analysis
    allow_failure: true
    needs: [flake8]
    before_script:
        - *python
        - pip install pyright
    script:
        - pyright src/ --outputjson > report_raw.json
    after_script:
        - cat report_raw.json
    artifacts:
        paths:
            - report_raw.json

code-quality:
    stage: static analysis
    allow_failure: true
    needs:
        - job: pyright
          artifacts: true
    image: node:21
    before_script:
        - npm i -g pyright-to-gitlab-ci
    script:
        - pyright-to-gitlab-ci --src report_raw.json --output report.json --base_path .
    artifacts:
        paths:
            - report.json
        reports:
            codequality: report.json

pylint:
    stage: static analysis
    script:
        - *python
        - pip install pylint
        - pylint src/scientific_plots --fail-under=9
        - pylint tests/ --fail-under=9

flake8:
    stage: static analysis
    script:
        - *python
        - pip install flake8
        - flake8 src
        - flake8 tests

vulture:
    stage: static analysis
    script:
        - *python
        - pip install vulture
        - python -m vulture --min-confidence 90 src/

mypy:
    stage: static analysis
    script:
        - *python
        - pip install mypy
        - mypy tests/
        - mypy src/scientific_plots
        - mypy src/

python-tests:
    stage: testing
    script:
        - *python
        - pip install pytest-cov
        - >
          pytest -v --cov=scientific_plots --cov-report term
          --cov-report xml:coverage.xml tests/
    coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml

build:
    stage: build
    script:
        - *python
        - pip install twine build
        - python -m build
        - python -m twine check dist/*

testpypi:
    stage: publish
    only:
        refs:
            - master
    script:
        - *python
        - pip install twine build
        - python -m build
        - TWINE_PASSWORD=${TEST_PYPI_TOKEN} TWINE_USERNAME=__token__ python -m twine upload --repository testpypi dist/*

gitlab-pypi:
    stage: publish
    needs: [testpypi]
    only:
        refs:
            - master
    script:
        - *python
        - pip install twine build
        - python -m build
        - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

pypi:
    stage: publish
    needs: [testpypi]
    only:
        refs:
            - master
    script:
        - *python
        - pip install twine build
        - python -m build
        - TWINE_PASSWORD=${PYPI_TOKEN} TWINE_USERNAME=__token__ python -m twine upload dist/*
