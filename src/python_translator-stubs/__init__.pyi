#!/usr/bin/env python3
"""
Stubs for the Python-Translator python module.
"""
from __future__ import print_function


class Translator():
    """
    Translator API.
    """

    def translate(self, input_str: str, lang1: str, lang2: str) -> str: ...
