#!/usr/bin/env python3
"""This is the stub-file for the window-functions in scipy. They can be used to
eliminate artifacts in the calculated Fourier-spectra."""
from scientific_plots.types_ import Vector


def kaiser(M: int, beta: float, sym: bool = True) -> Vector: ...
